#include "ContourObject.h"

ContourObject::ContourObject(std::vector<cv::Point> &contour)
{
    p_contour = contour;
    p_area = cv::contourArea(contour);
    assignDimensions();
    findCenter();
}

ContourObject::~ContourObject()
{
    //dtor
}

double ContourObject::getHeight() const
{
    return this->p_height;
}

double ContourObject::getWidth() const
{
    return this->p_width;
}

std::pair<double, double> ContourObject::getCenter() const
{
    return this->p_center;
}

double ContourObject::getArea() const
{
    return this->p_area;
}

void ContourObject::assignDimensions()
{
    double max_x, max_y = -99999;
    double min_x, min_y = 99999;
    for(auto i: p_contour) {
        if (max_x < i.x) {max_x = i.x;}
        if (max_y < i.y) {max_y = i.y;}
        if (min_x > i.x) {min_x = i.x;}
        if (min_y > i.y) {min_y = i.y;}
    }
    p_width = max_x - min_x;
    p_height = max_y - min_y;

}
void ContourObject::findCenter()
{
    auto moments = cv::moments(p_contour);
    double cX = moments.m10 / moments.m00;
    double cY = moments.m01 / moments.m00;
    p_center = std::pair<double,double>(cX, cY);

}

std::vector<cv::Point> ContourObject::getObjectContours() const {
    return p_contour;
}
