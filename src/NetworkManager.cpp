#include "NetworkManager.h"
#include <boost/asio.hpp>
#include <string>
#include <sstream>
using namespace boost::asio;
using ip::tcp;
using std::string;
using std::endl;

NetworkManager::NetworkManager(string ip, unsigned int port)
{
    this->port = port;
    this->ip = ip;
}

NetworkManager::~NetworkManager()
{
    //dtor
}

unsigned int NetworkManager::carOrPedestrian(std::string image_path) {
try {
    boost::asio::io_service io_service;
    //socket creation
     tcp::socket socket(io_service);
    //connection
     socket.connect( tcp::endpoint( boost::asio::ip::address::from_string(this->ip), this->port ));
    // dane ktore bedziemy przesylac
     const string msg = image_path;
     boost::system::error_code error;
     boost::asio::write( socket, boost::asio::buffer(msg), error );
     if( error ) {
        return 100;
     }
 // odpowiedz serwera
    boost::asio::streambuf receive_buffer;
    boost::asio::read(socket, receive_buffer, boost::asio::transfer_all(), error);
    if( error && error != boost::asio::error::eof ) {
        return 100;
    }
    else {
        const char* data = boost::asio::buffer_cast<const char*>(receive_buffer.data());
        std::stringstream strValue;
        strValue << data;
        unsigned int intValue;
        strValue >> intValue;
        if (intValue == 0 || intValue == 1) {return 0;}
        if (intValue == 2) {return 1;}
        return 100;
    }
    return 0;
    }
catch (...) {return 100;} //lapiemy bledy takie jak niedostepny serwer
}
