#include "MovementObject.h"
#include <experimental/filesystem>
MovementObject::MovementObject(std::pair<double, double> coordinates, double width, cv::Rect box)
{
    auto now = std::chrono::high_resolution_clock::now();
    this->timeArray.push_back(now);
    this->positionArray.push_back(coordinates);
    unsigned int red = std::rand() % 255;
    unsigned int green = std::rand() % 255;
    unsigned int blue = std::rand() % 255;
    this->color = cv::Scalar(red,green,blue);
    this->widthList.push_back(width);
    this->hasBeenClassified = false;
    this->isCar = false;
    this->latestBoundaries = box;
}

MovementObject::~MovementObject()
{
    //dtor
}

void MovementObject::updatePositionData(std::pair<double, double> coordinates, double width, cv::Rect box) {
    auto now = std::chrono::high_resolution_clock::now();
    this->timeArray.push_back(now);
    this->positionArray.push_back(coordinates);
    this->widthList.push_back(width);
    this->latestBoundaries = box;
}

double MovementObject::getDistance(std::pair<double, double> coordinates) {
    auto lastPos = this->positionArray.back();
    return std::sqrt(std::pow((coordinates.first - lastPos.first),2) + std::pow((coordinates.second - lastPos.second),2));
}

double MovementObject::getWidth() {
    double totalWidth = 0.0;
    double margin = 1200; //zaden obiekt nie moze miec takiej szerokosci
    unsigned int rejected = 0;
    for (auto width: this->widthList) {
        if (width>margin) {rejected+=1;}
        else {
            totalWidth += width;
        }
    }
    return totalWidth / (this->widthList.size() - rejected);
}

bool MovementObject::classify(cv::Mat image, NetworkManager &manager) {
    if (this->getLifetimeInMs() < 2000) {return false;} //ocena zajmuje czas i jest dosc kosztowna, bedziemy ja robic tylko dla obiektow widocznych powyzej 2 sekund
    if (this->hasBeenClassified) {return false;}
    cv::Mat objectOnlyImage = cv::Mat(image, latestBoundaries).clone();
    std::string path = std::experimental::filesystem::current_path();
    path+= "/toClassify.png";
    imwrite(path, objectOnlyImage );
    unsigned int result = manager.carOrPedestrian(path);
    if (result != 100) {
        this->isCar = !result;
        this->hasBeenClassified = true;
    }

    return true;
}
