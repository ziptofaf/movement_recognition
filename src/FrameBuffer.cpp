#include "FrameBuffer.h"

FrameBuffer::FrameBuffer()
{
    timer = Clock::now();
    deleteLastFrame = false;
}

FrameBuffer::~FrameBuffer()
{
    //dtor
}

void FrameBuffer::addCvFrame(Mat frame, unsigned int sizeThreshold) {
    auto current_time =  Clock::now();
    auto miliseconds = std::chrono::duration_cast<milliseconds>(current_time - timer);
    unsigned int deltaTime = miliseconds.count();
    timer = current_time;
    if (this->frames.size() == 0) {
        auto prev_frame = frame.clone();
        Frame movementFrame(frame, prev_frame, deltaTime, sizeThreshold);
        this->frames.push_back(movementFrame);
    }
    else {
        auto prev_frame = this->frames.back().visualFrame;
        Frame movementFrame(frame, prev_frame, deltaTime, sizeThreshold);
        this->frames.push_back(movementFrame);
    }
    if (this->deleteLastFrame){
        this->frames.erase(this->frames.begin());
    }
    else {
        checkIfBufferFull();
    }
}

void FrameBuffer::checkIfBufferFull() {
    unsigned int miliseconds=0;
    for (auto &i: this->frames) {
        miliseconds += i.msSinceLast;
        if (miliseconds > BUFFER_SIZE) {this->deleteLastFrame=true;}
    }
}
