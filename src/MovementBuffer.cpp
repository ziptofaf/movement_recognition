#include "MovementBuffer.h"
#include <algorithm>
#include <memory>
#include <climits>
#include <map>
MovementBuffer::MovementBuffer(unsigned int timeUntilDeletion, double minimumCoveredDistance)
{
    this->deletionPeriod = timeUntilDeletion;
    this->removedCars = 0;
    this->removedPedestrians = 0;
    this->removedObjects = 0;
    this->minimumCoveredDistance = minimumCoveredDistance;
}

MovementBuffer::~MovementBuffer()
{
    //dtor
}

void MovementBuffer::getOutOfRangeObjectsAndPurge()
{
    auto now = std::chrono::high_resolution_clock::now();
    unsigned int deletionPeriod = this->deletionPeriod;
    unsigned int initialCount = this->movingObjects.size();
    double minimumCoveredDistance = this->minimumCoveredDistance;
    if (initialCount == 0) {return;}

    std::vector<size_t> garbageObjectsIndices;

    for (size_t i=0; i< this->movingObjects.size(); i++) {
        auto milisecondsSinceLastUpdate = std::chrono::duration_cast<std::chrono::milliseconds>(now-movingObjects[i].getLastUpdate()).count();
        auto coveredDistance = movingObjects[i].getTotalCoveredDistance();
        if (milisecondsSinceLastUpdate > deletionPeriod && coveredDistance < minimumCoveredDistance) {
        garbageObjectsIndices.push_back(i);
        }
    }
    deleteIndices(garbageObjectsIndices);
    std::vector<size_t> objectsToAddToCount;


    for (size_t i=0; i< this->movingObjects.size(); i++) {
        auto milisecondsSinceLastUpdate = std::chrono::duration_cast<std::chrono::milliseconds>(now-movingObjects[i].getLastUpdate()).count();
        auto coveredDistance = movingObjects[i].getTotalCoveredDistance();
        if (milisecondsSinceLastUpdate > deletionPeriod && coveredDistance > minimumCoveredDistance) {
            objectsToAddToCount.push_back(i);
        }
    }
    updateCounters(objectsToAddToCount);
    deleteIndices(objectsToAddToCount);

}


void MovementBuffer::addToArray(std::pair<double, double> position, double width, double maximumDistance, cv::Rect box) {
    size_t index = 999999; //magiczna wartosc, oznacza ze nie znalezlismy dobrego obiektu
    double min_distance = std::numeric_limits<double>::infinity();
    for (size_t i = 0; i<this->movingObjects.size(); i++) {
        auto current_distance = this->movingObjects[i].getDistance(position);
        if (current_distance < min_distance && current_distance < maximumDistance) {
            index = i;
            min_distance = current_distance;
        }
    }
    if (index != 999999) {
        this->movingObjects[index].updatePositionData(position, width, box);
    }
    else {
        this->movingObjects.push_back(MovementObject(position, width, box));
    }

}

void MovementBuffer::deleteIndices(std::vector<size_t> &indicesToDelete) {
    //przesuwamy obiekty do usuniecia na koniec
    size_t pos = this->movingObjects.size()-1;
    auto tempObject = this->movingObjects[0];
    std::map<size_t, size_t> swaps; //potrzebne bo pozycja obiektu moze sie zmieniac
    for (auto i: indicesToDelete) {
        if (swaps.count(i) == 0) {
            tempObject = this->movingObjects[pos];
            this->movingObjects[pos] = this->movingObjects[i];
            this->movingObjects[i] = tempObject;
            swaps[pos] = i;
        }
        else {
            size_t swappedIndex = swaps[i];
            tempObject = this->movingObjects[pos];
            this->movingObjects[pos] = this->movingObjects[swappedIndex];
            this->movingObjects[i] = tempObject;
        }
        pos--;
    }
    this->movingObjects.erase(this->movingObjects.end() - indicesToDelete.size(), this->movingObjects.end());
}

void MovementBuffer::updateCounters(std::vector<size_t> &elems) {
    for (auto i: elems) {
        MovementObject &object = this->movingObjects[i];
        if (object.hasBeenClassified) {
            if (object.isCar) {this->removedCars+=1;}
            else {this->removedPedestrians+=1;}
        }
        else {
            this->removedObjects += 1;
        }
    }
}
