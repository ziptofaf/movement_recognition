#include "Frame.h"

Frame::Frame(Mat currentFrame, Mat previousFrame, unsigned int deltaTime, unsigned int areaRestriction) {

    std::vector<std::vector<Point> > cnts;
    Mat grayCurrent, grayPrevious, frameDelta, thresh, contourFrame;
    cvtColor(currentFrame, grayCurrent, COLOR_BGR2GRAY);    //zamienamy na grayscale
    cvtColor(previousFrame, grayPrevious, COLOR_BGR2GRAY);
    absdiff(grayCurrent, grayPrevious, frameDelta); //roznica klatkowa
    threshold(frameDelta, thresh, 20, 255, THRESH_BINARY); //binaryzacja
    dilate(thresh, thresh, Mat(), Point(-1,-1), 4); //dylacja, "rozproszenie"
    findContours(thresh, cnts, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);
    dilatedFrame = thresh;
    pFrameDelta = frameDelta;

         cnts.erase(std::remove_if(cnts.begin(), cnts.end(), [cnts, areaRestriction](const std::vector<Point> x)
    {
        return contourArea(x) < areaRestriction;
    }), cnts.end());

    for (auto i: cnts) {
        this->objectsArray.push_back(ContourObject(i));
        boundRect.push_back(cv::boundingRect(i));
    }
    this->msSinceLast = deltaTime;
    this->visualFrame = currentFrame.clone();
}

std::vector<std::vector<cv::Point>> Frame::getMovingObjects() {
    std::vector<std::vector<cv::Point>> cnts;
    for (auto i: this->objectsArray) {
        cnts.push_back(i.getObjectContours());
    }
    return cnts;
}


Frame::~Frame()
{
    //dtor
}
