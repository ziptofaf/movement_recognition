/*
* To compile: g++ opencv_test2.cpp -o opencv_test2 $(pkg-config --cflags --libs opencv)
*/
#include <opencv2/opencv.hpp>
#include <opencv2/tracking/tracking.hpp>
#include <opencv2/core/ocl.hpp>
#include <unistd.h>
#include <algorithm>
#include <thread>
#include <chrono>
#include <string>
#include <ctime>
#include <memory>
#include <iostream>
#include <opencv2/highgui.hpp>
#include "FrameBuffer.h"
#include "MovementBuffer.h"
#include "NetworkManager.h"

using namespace cv;
using namespace std;

int main(int argc, char **argv) {
    NetworkManager manager("127.0.0.1", 10000); //domyslnie w projekcie uzyjemy localhosta
    srand( time( NULL ) );
    //wybor formatu nagrania
    if (argc < 2) {
    std::cout << "Musisz podać czy chcesz obraz z kamery czy wideo (w tym drugim przypadku sciezke)\n";
    return 0;
    }
    VideoCapture* camera;
    unsigned int width = 1280;
    unsigned int height = 720;
    if (argv[1] == std::string("camera")) {
        camera = new VideoCapture(0);
        camera->set(3, width); //rozdzielczosc
        camera->set(4, height); //rozdzielczosc
    }
    else {
        camera = new VideoCapture(argv[1]);
        width = camera->get(3);
        height = camera->get(4);
    }
    unsigned int sizeThreshold = 3000;
    const unsigned int CLEANING_TIMER = 500; //500 domyslnie, co pol sekundy mozemy wywolac funkcje sprawdzajaca czy juz pora sprawdzic czy jakis pojazd jest do usuniecia z listy (ze przejechal)
    unsigned int msSum = 0; //resetujemy do 0 jak przekroczy CLEANING_TIMER

    FrameBuffer buffer;
    MovementBuffer movBuffer(500, width/3); //drugi argument to minimalny dystans jaki musi zostac pokonany (w pikselach) by obiekt sie liczyl

    const double averageCarWidth = 4.5; //w metrach
    const double maximumSpeed = 55.56; //metry na sekunde, ok. 200km/h

    Mat prevFrame, currentFrame;
    sleep(3); //czas na inicjalizacje

    camera->read(currentFrame);
    prevFrame = currentFrame.clone();
    Scalar color(0,0,255);
    while(camera->read(currentFrame)) {

        buffer.addCvFrame(currentFrame, sizeThreshold);
        for (size_t i = 0; i < buffer.getLastFrame().objectsArray.size(); i++) {
            auto object = buffer.getLastFrame().objectsArray[i];
            auto rect = buffer.getLastFrame().getMovingObjectsRectanges()[i];
            double carWidth = rect.width;
            movBuffer.addToArray(object.getCenter(), carWidth, 50.0, rect);
        }

        unsigned int miliseconds = buffer.frames.back().msSinceLast;
        msSum += miliseconds;
        if (msSum > CLEANING_TIMER)
        {
            movBuffer.getOutOfRangeObjectsAndPurge();
            msSum = 0;
        }
        auto movingObjects = movBuffer.getMovingObjectsArray();

        for (auto &i: (*movingObjects)) {
            if (i.getTotalCoveredDistance() > 10) { //10 pixeli przesuniecia, takie minimum zeby unikac anomalii
                i.classify(currentFrame, manager);
                for (size_t j=1; j<i.positionArray.size(); j++) {
                    line(currentFrame, Point(i.positionArray[j-1].first, i.positionArray[j-1].second), Point(i.positionArray[j].first, i.positionArray[j].second), i.color, 2);
                }
                auto last_coords = i.positionArray[i.positionArray.size()-1];
                putText(currentFrame, std::to_string(i.getTotalCoveredDistance()), Point(last_coords.first, last_coords.second), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,255,0),2);
                putText(currentFrame, std::to_string(i.getLifetimeInMs()), Point(last_coords.first, last_coords.second+20), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,255,255),2);
                double pixelsInMeter = i.getWidth() / averageCarWidth;
                double s = i.getTotalCoveredDistance() / pixelsInMeter;
                double t = i.getLifetimeInMs() / 1000;
                double kph = s/t * 3.6;
                putText(currentFrame, std::to_string(kph), Point(last_coords.first, last_coords.second+40), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,0,255),2);
                if (i.hasBeenClassified) {
                    if (i.isCar) {
                        putText(currentFrame, "samochod", Point(last_coords.first, last_coords.second+60), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,0,255),2);
                    }
                    else {
                        putText(currentFrame, "pieszy", Point(last_coords.first, last_coords.second+60), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,0,255),2);
                    }
                }
                //putText(currentFrame, std::to_string(i.getWidth()), Point(last_coords.first, last_coords.second+60), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,0,255),2);
            }
        }
        rectangle(currentFrame, Point(0,0), Point(400,300), Scalar(0,0,0), -1);
        putText(currentFrame, std::to_string(sizeThreshold), Point(10, 20), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,0,255),2);
        putText(currentFrame, std::to_string(miliseconds), Point(10, 100), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0,0,255),2);
        putText(currentFrame, ("Niesklasyfikowane obiekty: " + std::to_string(movBuffer.removedObjects)), Point(10, 200), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,255,255),2);
        putText(currentFrame, ("Pojazdy: " + std::to_string(movBuffer.removedCars)), Point(10, 240), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,255,255),2);
        putText(currentFrame, ("Piesi: " + std::to_string(movBuffer.removedPedestrians)), Point(10, 280), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(255,255,255),2);
        for (auto i: buffer.getLastFrame().objectsArray) {
            circle(currentFrame, Point(i.getCenter().first, i.getCenter().second), 8, Scalar(0,255,0));
        }


        //drawContours(currentFrame, movingObjects, -1, color, 1);
        for (auto box : buffer.getLastFrame().getMovingObjectsRectanges()) {
            Scalar color = Scalar( 120, 120, 200 );
            rectangle( currentFrame, box.tl(), box.br(), color, 2, 8, 0 );
        }
        imshow("Pomiar", currentFrame);
        unsigned int pressedKey = waitKey(1);
        switch(pressedKey) {
        case 27: //escape
            goto break_point;
            break;
        case 171: //plus
            sizeThreshold += 100;
            break;
        case 173: //minus
            if (sizeThreshold > 0){sizeThreshold -=100;}
            break;
        }
    }
    break_point:
    delete camera;
    return 0;
}

