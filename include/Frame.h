#ifndef FRAME_H
#define FRAME_H
#include <vector>
#include <algorithm>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/tracking/tracking.hpp>
#include <opencv2/core/ocl.hpp>
#include "ContourObject.h"
using namespace cv;

class Frame
{
    public:
        Frame(Mat currentFrame, Mat previousFrame, unsigned int deltaTime, unsigned int areaRestriction=0);
        virtual ~Frame();
        std::vector<ContourObject> objectsArray;
        unsigned int msSinceLast;
        Mat visualFrame;
        std::vector<std::vector<cv::Point>> getMovingObjects();
        inline Mat getDilated() {return dilatedFrame;}
        inline Mat getDifference() {return pFrameDelta;}
        inline std::vector<cv::Rect>& getMovingObjectsRectanges() {return boundRect;}
    protected:

    private:
    Mat dilatedFrame;
    Mat pFrameDelta;
    std::vector<cv::Rect> boundRect;

};

#endif // FRAME_H
