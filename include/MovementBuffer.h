#ifndef MOVEMENTBUFFER_H
#define MOVEMENTBUFFER_H
#include "MovementObject.h"
const unsigned int ERRATIC_THRESHOLD=300;
class MovementBuffer
{
    public:
        MovementBuffer(unsigned int timeUntilDeletion, double minimumCoveredDistance);
        ~MovementBuffer();
        void addToArray(std::pair<double, double> position, double width, double maximumDistance, cv::Rect box);
        void getOutOfRangeObjectsAndPurge();
        unsigned int removedObjects;
        unsigned int removedCars;
        unsigned int removedPedestrians;
        inline std::vector<MovementObject>* const getMovingObjectsArray() {return &movingObjects;}
    protected:

    private:
    std::vector<MovementObject> movingObjects;
    unsigned int deletionPeriod;
    unsigned int minimumCoveredDistance;
    bool shouldBeDeleted(MovementObject object);
    void deleteIndices(std::vector<size_t> &indicesToDelete);
    void updateCounters(std::vector<size_t> &elems);
};

#endif // MOVEMENTBUFFER_H
