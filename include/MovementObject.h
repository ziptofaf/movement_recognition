#ifndef MOVEMENTOBJECT_H
#define MOVEMENTOBJECT_H
#include <vector>
#include <chrono>
#include <cmath>
#include <opencv2/opencv.hpp>
#include "ContourObject.h"
#include "NetworkManager.h"
class MovementObject
{
    public:
        MovementObject(std::pair<double, double> coordinates, double width, cv::Rect box);
        ~MovementObject();

        double getDistance(std::pair<double, double> coordinates);
        void updatePositionData(std::pair<double, double> coordinates, double width,cv::Rect box);
        inline std::chrono::high_resolution_clock::time_point getLastUpdate() {
            return this->timeArray.back();
        }
        inline unsigned int getLifetimeInMs() {
            return std::chrono::duration_cast<std::chrono::milliseconds>(this->timeArray.back() - this->timeArray.front()).count();
        }
        inline double getTotalCoveredDistance() {
            auto first = positionArray[0]; //zakladamy ze pojazd przesuwa sie glownie w jednej przestrzeni
            auto last = positionArray[positionArray.size()-1];
            return std::sqrt(std::pow((first.first - last.first),2) + std::pow((first.second - last.second),2));
        }
        std::vector<std::pair<double, double>> positionArray;
        cv::Scalar color;
        double getWidth();
        bool hasBeenClassified;
        bool isCar;
        bool classify(cv::Mat image, NetworkManager &manager);
    protected:

    private:

        std::vector<std::chrono::high_resolution_clock::time_point> timeArray;
        std::vector<double> widthList;
        cv::Rect latestBoundaries;
};

#endif // MOVEMENTOBJECT_H
