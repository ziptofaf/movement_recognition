#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H
#include <string>

class NetworkManager
{
    public:
        NetworkManager(std::string ip, unsigned int port);
        unsigned int carOrPedestrian(std::string image_path); //zwraca 0 dla pojazdow, 1 dla pieszych, 100 dla bledow
        virtual ~NetworkManager();

    protected:

    private:
        std::string ip;
        unsigned int port;
};

#endif // NETWORKMANAGER_H
