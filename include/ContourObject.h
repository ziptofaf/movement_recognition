#ifndef CONTOUROBJECT_H
#define CONTOUROBJECT_H
#include <vector>
#include <opencv2/opencv.hpp>

class ContourObject
{
    public:
        ContourObject(std::vector<cv::Point> &contour);
        virtual ~ContourObject();
        double getArea() const;
        double getHeight() const;
        double getWidth() const;
        std::pair<double, double> getCenter() const;
        std::vector<cv::Point> getObjectContours() const;
    protected:

    private:
    std::vector<cv::Point> p_contour;
    std::pair<double, double> p_center;
    double p_width;
    double p_height;
    double p_area;

    void assignDimensions();
    void findCenter();
};

#endif // CONTOUROBJECT_H
