#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H
#include "Frame.h"
#include <list>
#include <chrono>
#include <opencv2/opencv.hpp>
using namespace cv;
typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::milliseconds milliseconds;
const unsigned int BUFFER_SIZE = 5000;
class FrameBuffer
{
    public:
        FrameBuffer();
        virtual ~FrameBuffer();
        std::list<Frame> frames;
        void addCvFrame(Mat frame, unsigned int sizeThreshold);
        inline Frame& getLastFrame(){return this->frames.back();}
    protected:

    private:
         Clock::time_point timer;
         bool deleteLastFrame;
         void checkIfBufferFull();
};

#endif // FRAMEBUFFER_H
